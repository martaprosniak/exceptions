import java.util.Arrays;

public class ArrayOfInts {

    private int [] tab; //zadeklarowana tablica

    public ArrayOfInts(int... values){ //varax - można zapisać ... albo []; zmienna ilość parametrów jednego typu
        tab = values; //przypisanie do tablicy tab wartości

    }

    public int get(int idx) throws WrongIndexException {
        checkOtOfBounds(idx);
        return tab[idx];
    }

    private void checkOtOfBounds(int idx) throws WrongIndexException {
        if (isOutOfBounds(idx)) {
            String message = "Podałeś indeks spoza zakresu tablicy \n" +
                    "Podany index: " + idx + "\n" +
                    "Min. index: 0 " + "\n" +
                    "Max. index: " + (tab.length - 1);
            throw new WrongIndexException(message); //konstruktor nowego wyjątku
        }
    }

    private boolean isOutOfBounds(int idx) {
        return idx < 0 || idx >= tab.length;
    }

    public void set (int idx, int value) throws WrongIndexException {
        checkOtOfBounds(idx);
        tab[idx] = value;
    }

    public void printAll() {
        System.out.println(Arrays.toString(tab));
    }
}
