public class ArayApp {

    public static void main(String[] args) {

        ArrayOfInts a = new ArrayOfInts(1,2,3,4,5,6);
        a.printAll();

        /*
        Pierwszy wyjątek - metoda get
         */
        try {
            a.get(7);
        } catch (WrongIndexException ex) {
            //ex.printStackTrace(); //wyświetl stos wywowłań
            //System.out.println( ex.getCause()); //zwraca throwable
            System.out.println(ex.getMessage());
        }

        /*
        Drugi wyjątek - metoda set
         */
        try {
            a.set(10, 5);
        }
        catch (WrongIndexException ex){
            System.out.println(ex.getMessage());
        }
    }

}
