public class Exceptions {

    private static String s;

    public static void main(String[] args) {
        //System.out.println(2/0); - runtime exception

        int a [] = {1,2,3,4,5};
        try {
            System.out.println(a[666]);
        } catch (RuntimeException e) {
            System.out.println("Zły indeks");
            System.out.println(a[0]);
        }

        /*
        System.out.println(Integer.parseInt("dwanaście")); //przetwarza string w int

        W sytuacji, gdy użytkownik wpisze wiek słownie, wyrzuci się wyjątek (NumberFormatException)
         */

        System.out.println(s.length()); //wyrzuci NullPointerException
    }

}
