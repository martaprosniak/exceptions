public class WrongIndexException extends Exception {

    public WrongIndexException (String message) { //konstruktor dziedziczący po Exception
        super(message);
    }

}
